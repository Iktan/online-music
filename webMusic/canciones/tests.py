from django.test import TestCase
from canciones.models import Cancion
from canciones.forms import CancionForm

class cancionesTest(TestCase):
    def test_verPaginaCanciones(self):
        response = self.client.get('')
        self.assertTrue(response.status_code, 200)
    
    def test_verificarTemplateCanciones(self):
        response = self.client.get('')
        self.assertTemplateUsed(response, 'canciones.html')
    
    def test_verPaginaNuevaCancion(self):
        response = self.client.get('/nueva')
        self.assertEquals(response.status_code, 200)
    
    def test_verificarTemplateNuevaCancion(self):
        response = self.client.get('/nueva')
        self.assertTemplateUsed(response, 'nuevaCancion.html')

    def test_guardarCancion(self):
        cancion = Cancion.objects.create(
            id = '290195',
            nombre = 'I want to break free',
            artista = 'Queen',
            album = 'A nigth at the opera',
            precio = '50',
            
        )
        form = CancionForm(
            data={
                'id': cancion.id,
                'nombre': cancion.nombre,
                'artista': cancion.artista,
                'album': cancion.album,
                'precio': cancion.precio
            }
        )
        if form.is_valid():
            new_Cancion = form.save()
            self.assertEqual(new_Cancion, Producto.objects.all()[290195])
    
    def test_elimarCancion(self):
        cancion = Cancion.objects.create(
            id = '290195',
            nombre = 'I want to break free',
            artista = 'Queen',
            album = 'A nigth at the opera',
            precio = '50',
            
        )
        form = CancionForm(
            data={
                'id': cancion.id,
                'nombre': cancion.nombre,
                'artista': cancion.artista,
                'album': cancion.album,
                'precio': cancion.precio
            }
        )
        if form.is_valid():
            new_Cancion = form.save()
            new_Cancion.delete()
            self.assertFalse(new_Cancion, Producto.objects.all()[290195])
