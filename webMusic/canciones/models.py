from django.db import models

class Cancion(models.Model):
    id= models.IntegerField('id cancion', primary_key=True)
    nombre = models.CharField('Nombre',max_length=100)
    artista = models.CharField('Artista',max_length=40)
    album = models.CharField('Album',max_length=50)
    precio = models.IntegerField('Precio')

    def __str__(self):
        return self.nombre+" "+artista
