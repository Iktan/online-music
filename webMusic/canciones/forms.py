from django import forms
from .models import Cancion

class CancionForm(forms.ModelForm):

    class Meta:
        model = Cancion
        fields = ('id','nombre','artista','album','precio')


        error_messages = {
            'id' :{'required' : 'Debes ingresar un Identificador'},
            'nombre': {'required': 'Debes ingresar un nombre'},
            'artista': {'required': 'Debes ingresar una descripcion'},
            'album': {'required': 'Debes ingresar un precio'},
            'precio': {'required': 'Debes ingresar un año'},
            
        }