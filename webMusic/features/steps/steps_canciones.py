from behave import given, when, then
from django.conf import settings
from selenium import webdriver
from time import sleep


@given(u'que ingreso la direccion "{url}"')
def step_impl(context,url):
    context.driver = webdriver.Firefox()
    context.url = url
    sleep(4)

@when(u'se muestra la pagina')
def step_impl(context):
    context.driver.get(context.url)
    sleep(10)

@then(u'puedo ver las canciones existentes')
def step_impl(context):
    sleep(5)
    assert context.driver.find_element_by_id('listaCanciones')

@when(u'ingreso los datos de la cancion')
def step_impl(context):
    context.driver.find_element_by_xpath('//*[@id="id_id"]').send_keys('8762')
    sleep(1)
    context.driver.find_element_by_xpath('//*[@id="id_nombre"]').send_keys('Dracarys')
    sleep(1)
    context.driver.find_element_by_xpath('//*[@id="id_artista"]').send_keys('Dany')
    sleep(1)
    context.driver.find_element_by_xpath('//*[@id="id_album"]').send_keys('Snow')
    sleep(1)
    context.driver.find_element_by_xpath('//*[@id="id_precio"]').send_keys('100')
    sleep(1)


@when(u'presiono el boton de guardarNueva')
def step_impl(context):
    context.driver.find_element_by_xpath('//*[@id="guardarNueva"]').click()
    sleep(2)


@then(u'la cancion que acabo de agregar')
def step_impl(context):
    sleep(5)
    assert context.driver.find_element_by_id('8762')
   