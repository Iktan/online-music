from django.shortcuts import render, redirect
from canciones.models import Cancion
from canciones.forms import CancionForm

def lista_canciones(context):
    canciones = Cancion.objects.all()
    return render(context, 'canciones.html',{'canciones':canciones})

def nuevaCancion(context):
    if (context.method == 'POST'):
       form = CancionForm(context.POST)
       if form.is_valid():
           cancion = form.save()
           cancion.save()

           return redirect('canciones')

    else:
        form = CancionForm()
    return render(context, 'nuevaCancion.html',{'formulario':form,'accion':'Nueva'})

def editarCancion(request, id):
    cancion = Cancion.objects.get(pk=id)
    if request.method == "POST":
        form = CancionForm(request.POST, instance=cancion)
        if form.is_valid():
            cancion = form.save()
            cancion.save()
            return redirect('canciones')
    else:
        form = CancionForm(instance=cancion)
    return render(request, 'nuevaCancion.html', {'formulario': form,'accion':'Editar'})

def eliminarCancion(request):
    id = request.POST.get('id',None)
    cancion = Cancion.objects.get(pk=id)
    cancion.delete()
    return redirect('canciones')
