Característica: Canciones
    Como usuario de webMusic
    Quiero agregar una Cancion
    Para tener un amplio repertorio


    Escenario: Ver la lista de canciones
        Dado que ingreso la direccion "http://localhost:8300"
        Cuando se muestra la pagina
        Entonces puedo ver las canciones existentes
    
    Escenario: Agregar una cancion
        Dado que ingreso la direccion "http://localhost:8300/nueva"
        Cuando se muestra la pagina
        y ingreso los datos de la cancion
        y presiono el boton de guardarNueva
        Entonces puedo ver las canciones existentes
        y la cancion que acabo de agregar